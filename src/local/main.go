package main

import (
	"fmt"
	"strings"
)
import "os"
import "bufio"

func main() {

	const scanBuffer = 1000000
	const letterQty int = 26
	const asciiOffset = 65
	const asciiZ = 90

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, scanBuffer), scanBuffer)

	/*
		var L int
		scanner.Scan()
		fmt.Sscan(scanner.Text(),&L)

		var H int
		scanner.Scan()
		fmt.Sscan(scanner.Text(),&H)
	*/

	L := 4
	H := 5

	/*
		scanner.Scan()
		T := strings.ToUpper(scanner.Text())
	*/

	fmt.Println("Please enter your text to be converted and then press enter:")
	scanner.Scan()
	T := strings.ToUpper(scanner.Text())

	abcArr := [letterQty]string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "W", "X", "Y", "Z", "?"}
	arrLength := L * letterQty
	artArr := make([]string, H)
	var outputString string

	fmt.Println("L:         ", L)
	fmt.Println("H:         ", H)
	fmt.Println("arrLength: ", arrLength)
	fmt.Println("abcArr:    ", abcArr)

	for i := 0; i < H; i++ {
		/*
			scanner.Scan()
			artArr[i] = scanner.Text()
			fmt.Fprintln(os.Stderr, artArr[i])
		*/

		artArr[0] = " #  ##   ## ##  ### ###  ## # # ###  ## # # #   # # ###  #  ##   #  ##   ## ### # # # # # # # # # # ### ### "
		artArr[1] = "# # # # #   # # #   #   #   # #  #    # # # #   ### # # # # # # # # # # #    #  # # # # # # # # # #   #   # "
		artArr[2] = "### ##  #   # # ##  ##  # # ###  #    # ##  #   ### # # # # ##  # # ##   #   #  # # # # ###  #   #   #   ## "
		artArr[3] = "# # # # #   # # #   #   # # # #  #  # # # # #   # # # # # # #    ## # #   #  #  # # # # ### # #  #  #       "
		artArr[4] = "# # ##   ## ##  ### #    ## # # ###  #  # # ### # # # #  #  #     # # # ##   #  ###  #  # # # #  #  ###  #  "
	}

	fmt.Println(os.Stderr, "T (text to process): ", T)

	for i := 0; i < H; i++ { // Height
		outputString = ""
		for j := 0; j < len(T); j++ { // Letter

			letterAscii := int(T[j])
			var letterIdx int

			if letterAscii >= asciiOffset && letterAscii <= asciiZ {
				letterIdx = letterAscii - asciiOffset
			} else {
				letterIdx = letterQty
			}

			letterIdx *= L
			wordSlice := artArr[i][letterIdx : letterIdx+L]
			/*
			   fmt.Fprintln(os.Stderr, "letterAscii: ", letterAscii)
			   fmt.Fprintln(os.Stderr, "letterIndex: ", letterIdx)
			   fmt.Fprintln(os.Stderr, "wordSlice:   ", wordSlice)
			*/
			outputString += wordSlice
		}
		fmt.Println(outputString)
	}

}
